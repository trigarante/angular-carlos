import {Component, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Asignacion} from '../../interfaces/asignacion.model';
import {AsignacionesService} from '../../services/asignaciones.service';
import {Grupo} from '../../interfaces/grupo.model';
import {Router} from '@angular/router';
import {Empleado} from '../../interfaces/empleados.model';
import {MessageService} from 'primeng/api';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-modal-index',
  templateUrl: './modal-index.component.html',
  styleUrls: ['./modal-index.component.css'],
  providers: [MessageService]
})
export class ModalIndexComponent implements OnInit {
  @Input() idEmpleado = 0;
  @Input() subarea = 0;
  correos: Asignacion[];
  empleados: Empleado[];
  grupos: Grupo[];
  registerForm: FormGroup;
  userParameter = 0;
  submitted = false;

  constructor(public activeModal: NgbActiveModal, private asignacionesService: AsignacionesService,
              private router: Router, private messageService: MessageService, private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.getCorreos();
    this.getGrupos();
    this.registerForm = this.formBuilder.group({
      usuario: ['', Validators.required],
      id_grupo: ['', Validators.required],
      id_empleado: [this.idEmpleado, [Validators.required]]
    });
  }

  getCorreos() {
    this.asignacionesService.getCorreos(this.subarea).subscribe(data => this.correos = data);
  }

  getGrupos() {
    this.asignacionesService.getGrupos().subscribe(data => this.grupos = data);
  }

  get f() {
    return this.registerForm.controls;
  }

  guardarUsuario() {
    console.warn(this.registerForm.value);
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }
    this.asignacionesService.createUser(this.registerForm.value).subscribe((result) => {
      this.activeModal.close();
      this.router.navigate(['asignaciones'], {queryParams: {mensaje: 'exito'}});
    });
  }
}
