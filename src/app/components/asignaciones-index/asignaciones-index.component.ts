import {Component,  OnInit} from '@angular/core';
import {AsignacionesService} from '../../services/asignaciones.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Empleado} from '../../interfaces/empleados.model';
import {ModalIndexComponent} from '../modal-index/modal-index.component';
import {Asignacion} from '../../interfaces/asignacion.model';
import {ActivatedRoute} from '@angular/router';
import {filter} from 'rxjs/operators';
import {MessageService} from 'primeng/api';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-asignaciones-index',
  templateUrl: './asignaciones-index.component.html',
  styleUrls: ['./asignaciones-index.component.css'],
  providers: [MessageService, NgbActiveModal]

})
export class AsignacionesIndexComponent implements OnInit {
  empleados: Empleado[];
  correos: Asignacion[];
  cols: any[];
  tipoUsuarios: any[];
  mensaje: string;

  constructor(private asignacionesService: AsignacionesService, private bsModalService: BsModalService, private modalService: NgbModal,
              private route: ActivatedRoute, private messageService: MessageService) {
  }

  getEmpleados() {
    this.asignacionesService.getEmpleadosList().subscribe(data => this.empleados = data);
  }

  openModal(id, idSubarea) {
    console.log(id, idSubarea);
    const modal = this.modalService.open(ModalIndexComponent);
    modal.componentInstance.idEmpleado = id;
    modal.componentInstance.subarea = idSubarea;
  }

  ngOnInit() {
    this.route.queryParams.pipe(filter(params => params.mensaje))
      .subscribe(params => {
        console.log(params);
        this.mensaje = params.mensaje;
        if (this.mensaje === 'exito') {
          this.messageService.add({severity: 'success', summary: 'Usuario Creado'});
        }
        console.log(this.mensaje); // popular
      });
    this.cols = [
      {field: 'nombre', header: 'Nombre'},
      {field: 'email', header: 'Email'},
      {field: 'telefono_movil', header: 'Telefono'},
      {field: 'puesto', header: 'Puesto'},
      {field: 'tipo_usuario', header: 'Tipo de Usuario'},
      {field: 'id_tipo', header: 'Asignar'},
    ];
    this.tipoUsuarios = [
      {label: 'Todos', value: ''},
      {label: 'Ejecutivo', value: 'Ejecutivo'},
      {label: 'Administrativo', value: 'Administrativo'}
    ];

    this.getEmpleados();
  }
}
