import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignacionesIndexComponent } from './asignaciones-index.component';

describe('AsignacionesIndexComponent', () => {
  let component: AsignacionesIndexComponent;
  let fixture: ComponentFixture<AsignacionesIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignacionesIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignacionesIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
